# Numerius is a virtual currency

# Copyright (C) 2019 Fabricius Flamen <fabricius@envs.net>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import random
import sqlite3
import argparse
import datetime as dt

parser = argparse.ArgumentParser(description='Numerius is a virtual currency')
parser.add_argument('-f', '--flip', type=str,
                    help='flip a coin')
parser.add_argument('-n', '--numerius', type=int,
                    help='numerius to bet')

group = parser.add_mutually_exclusive_group()
group.add_argument('--init', action='store_true',
                   help='initialize new account')
group.add_argument('-r', '--redeem', action='store_true',
                   help='redeem coins when bankrupt')
group.add_argument('-i', '--info', action='store_true',
                   help='account details')
group.add_argument('--contact', action='store_true',
                   help='contact details')
args = parser.parse_args()

def contact():
    print("Visit gemini://tilde.pink/~fabricius")
    sys.exit(0)

def error(conn):
    print("Try again or use --contact.")
    conn.close()
    sys.exit(1)

def user_exists(uid, curs):
    curs.execute("SELECT id FROM bank WHERE id = ?;", (uid,))
    return(len(curs.fetchone()))

def init(uid, curs, conn):
    curs.execute("INSERT INTO bank (id, bal, creation, access) \
    VALUES (?, ?, ?, ?);", (uid, 100, dt.datetime.utcnow().isoformat(), 0))
    conn.commit()

    # check if account was created
    account = user_exists(uid, curs)
    if account == 0:
        print("Error: Account was not created.")
        error(conn)
    elif account == 1:
        print("Account initialized successfully.")
        conn.close()
        sys.exit(0)
    else:
        print("Error: Unexpected result.")
        error(conn)

def check_bal(uid, curs):
    curs.execute("SELECT bal FROM bank WHERE id = ?;", (uid,))
    return(curs.fetchone()[0])

def update_bal(balance, new_bal, uid, curs, conn):
    # transfer money to users account
    curs.execute("UPDATE bank SET bal = ?, access = ? \
    WHERE id = ?;", (new_bal, dt.datetime.utcnow().isoformat(), uid))
    conn.commit()

    # check if money was transferred
    cur_bal = check_bal(uid, curs)
    if cur_bal == new_bal:
        print(f"Your account balance is {cur_bal} Numerius.")
    elif cur_bal == balance:
        print("Error: Transaction failed.")
        error(conn)
    else:
        print("Error: Unexpected result.")
        error(conn)

def flip(numerius, h_t, balance, uid, curs, conn):
    if numerius > balance:
        print(f"Your account balance is {balance} Numerius.")
        print("Error: You cannot bet more Numerius than your balance.")
        error(conn)
    elif numerius <= 0:
        print(f"Error: You cannot bet {numerius} Numerius.")
        print("Error: Pass a positive integer greater than 0.")
        error(conn)
    else:
        coin_flip = random.choice(["heads", "tails"])
        if h_t == coin_flip:
            print(f"You win {numerius} Numerius.")
            new_bal = balance + numerius
            update_bal(balance, new_bal, uid, curs, conn)
        else:
            print(f"You lose {numerius} Numerius.")
            new_bal = balance - numerius
            update_bal(balance, new_bal, uid, curs, conn)

if __name__ == '__main__':
    print("Numerius")
    print("Copyright (C) 2019 Fabricius Flamen <fabricius@envs.net>")
    print("License: GNU GPL version 3 or later <https://www.gnu.org/licenses/\n")

    if args.contact:
        contact()

    # connect to sqlite
    database = '/home/fabricius/projects/fabricius/bank.db'
    conn = sqlite3.connect(database)
    curs = conn.cursor()

    # get user id and check if user exists
    uid = os.getuid()
    account = user_exists(uid, curs)

    if account == 0:
        if args.init:
            init(uid, curs, conn)
        else:
            print("Error: Account doesn't exist, use --init to create an account.")
            conn.close()
            sys.exit(1)
    else:
        if args.init:
            print("Error: Account already initialized.")

    # get user account balance
    balance = check_bal(uid, curs)

    if args.info:
        print(f"Your account balance is {balance} Numerius.")

    if args.redeem:
        if balance == 0:
            update_bal(balance, 100, uid, curs, conn)
        else:
            print(f"Your account balance is {balance} Numerius.")
            print("Error: Account balance should be 0 Numerius to redeem.")

    if args.flip:
        if args.flip == "heads" or args.flip == "tails":
            # if user has not passed numerius to bet then exit
            if args.numerius:
                flip(args.numerius, args.flip, balance, uid, curs, conn)
            else:
                print("Error: Pass Numerius to bet in --numerius.")
                error(conn)
        else:
            print("Error: You must choose heads or tails.")
            error(conn)

    conn.close()
